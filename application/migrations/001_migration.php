<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Migration_migration extends CI_Migration {
public function up() {
        $this->db->trans_begin();
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nama varchar(100) NOT NULL',
            'nomor_hp varchar(16) NOT NULL UNIQUE',
            'email varchar(100) NOT NULL',
            'password varchar(32) NOT NULL',
            'lokasi varchar(100) NOT NULL',
            'foto_profil varchar(100) NOT NULL',
            'role int(3) NOT NULL',
        ));
        $this->dbforge->add_key('id');
        $this->dbforge->create_table('user', FALSE, array('ENGINE' => 'InnoDB'));

        // START Token Auth

        $this->dbforge->add_field(array(
            'token' => array(
                'type' => 'VARCHAR',
                'constraint' => 32,
                'unique'=>TRUE
            ),
            'id_user'=>array(
                'type'=>'INT',
                'constraint'=>11,
                'NULL'=>FALSE,
                'unsigned'=>TRUE
            ),
            'ip_address varchar(15) NOT NULL',
            'timestamp_created timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()',
            'timestamp_expired timestamp NULL DEFAULT NULL',
        ));
        $this->dbforge->add_key('id');
        $this->dbforge->create_table('token_auth', FALSE, array('ENGINE' => 'InnoDB'));
        $this->db->query('ALTER TABLE token_auth ADD FOREIGN KEY (id_user) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE;');

        // START Iklan

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "judul varchar(100) NOT NULL",
            "deskripsi longtext NOT NULL",
            "jenis_iklan enum('motor''sepeda') NOT NULL",
            "harga varchar(12) NOT NULL",
            "kondisi enum('bekas''baru') NOT NULL",
            "merek varchar(100) NOT NULL",
            "tahun year(4) NOT NULL",
            "timestamp_created timestamp NOT NULL DEFAULT current_timestamp()",
        ));

        $this->dbforge->add_key('id');
        $this->dbforge->create_table('iklan', FALSE, array('ENGINE' => 'InnoDB'));

        // START spesifikasi_iklan

        $this->dbforge->add_field(array(
            'id_iklan' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            "spesifikasi varchar(100) NOT NULL",
            "value varchar(255) NOT NULL",
            'UNIQUE KEY `id_iklan` (`id_iklan`,`spesifikasi`)',
            'CONSTRAINT `spesifikasi_iklan_ibfk_1` FOREIGN KEY (`id_iklan`) REFERENCES `iklan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE'
        ));

        $this->dbforge->create_table('spesifikasi_iklan', FALSE, array('ENGINE' => 'InnoDB'));

        // START whistlist

        $this->dbforge->add_field(array(
            'id_iklan' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'id_user' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            "timestamp_created timestamp NOT NULL DEFAULT current_timestamp()",
            'UNIQUE KEY `id_user` (`id_user`,`id_iklan`)',
            'CONSTRAINT `whistlist_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE',
            'CONSTRAINT `whistlist_ibfk_2` FOREIGN KEY (`id_iklan`) REFERENCES `iklan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE'
        ));

        $this->dbforge->add_key('id_iklan');
        $this->dbforge->create_table('whistlist', FALSE, array('ENGINE' => 'InnoDB'));

        if($this->db->trans_status()===true){
            $this->db->trans_commit();
        }
        else{
            $this->db->trans_rollback();
        }
    }
    public function down() {
        $this->dbforge->drop_table('user');
    }
}