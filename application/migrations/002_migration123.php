<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Migration_migration123 extends CI_Migration {
public function up() {
        $this->db->trans_begin();
        $fields = array(
            'username' => array(
                'type' => 'VARCHAR',
                'constraint'=>100,
                'UNIQUE'=>TRUE,
                'AFTER email'
            )
        );
        $this->dbforge->add_column('user', $fields);

        $fields = array(
            'role' => array(
                'type' => 'VARCHAR',
                'constraint'=>100
            )
        );
        $this->dbforge->add_column('token_auth', $fields);

        $this->db->query('ALTER TABLE `user` CHANGE `role` `role` ENUM("user","admin") NOT NULL;');

        if($this->db->trans_status()===true){
            $this->db->trans_commit();
        }
        else{
            $this->db->trans_rollback();
        }
    }
    public function down() {
        $this->dbforge->drop_table('user');
    }
}