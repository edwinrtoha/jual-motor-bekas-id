<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_hook {
    public function migration(){
        $ci=&get_instance();

        $ci->load->library('migration');
        if (!$ci->migration->current()) {
            show_error($ci->migration->error_string());
            http_response_code(500);
        } else {
        }
    }
}
?>