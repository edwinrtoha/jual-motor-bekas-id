<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class whistlist extends CI_Controller {
    var $CI = NULL;
	public function __Construct(){
		$this->CI =& get_instance();
        parent ::__construct();
        header('Content-type: application/json');
        $this->load->model('api_model');
        $this->api_model->get_allow_origin();
        $this->load->model('whistlist_model');
	}

	public function index(){
		switch($this->input->method()){
            case 'post':
                $data=$this->input->post();
                if($this->whistlist_model->tambah($data)==1){
                    echo $this->api_model->response(array($data));
                }
                else{
                    echo 123;
                    echo $this->api_model->response(array(array('error'),200));
                }
                break;
            case 'get':
                $query['param']=$this->input->get();
                if(isset($query['param']['limit_result'])){
                    $query['limit']=$query['param']['limit_result'];
                    unset($query['param']['limit_result']);
                }
                if($query=$this->whistlist_model->get($query)){
                    if($query->num_rows()){
                        $response=array($query->result(),200);
                    }
                    else{
                        $response=array(array(),204);
                    }
                }
                else{
                    $response=array(array(),400);
                }
                echo $this->api_model->response($response);
                break;
            default:
                http_response_code(405);
        }
    }

    public function edit(){
        switch($this->input->method()){
            case 'post':
                if(!$this->input->post('id')) http_response_code(404);
                $data=array(
                    'param'=>$this->input->get(),
                    'data'=>$this->input->post()
                );
                if($this->whistlist_model->edit($data)){
                    echo $this->api_model->response(array($data));
                }
                else{
                    echo $this->api_model->response(array($data));
                }
                break;
            default:
                http_response_code(405);
        }
    }
    
    public function delete(){
        $data=$this->input->post();
        print_r($data);
        if($this->whistlist_model->delete($data)){
            echo $this->api_model->response(array(array('Berhasil'),200));
        }
        else{
            echo $this->api_model->response(array(array('error'),200));
        }
    }
}