<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {
    var $CI = NULL;
    var $user_session=array();
	public function __Construct(){
		$this->CI =& get_instance();
        parent ::__construct();
        header('Content-type: application/json');
        $this->load->model('api_model');
        $this->api_model->get_allow_origin();
        $this->load->model('akun_model');
        
        if(!empty($this->api_model->getToken())){
            $data=array(
                'param'=>array(
                    'token'=>$this->api_model->getToken()
                ),
                'limit'=>1
            );
            if($this->api_model->getTokenDetail($data)==true){
                $this->user_session=$data[0];
            }
            else{
                echo $this->api_model->response(array(array('Invalid Token'),500));
                exit;
            }
        }
    }
    
    public function index($sub=NULL){
        switch($this->input->method()){
            case 'get':
                $data=$this->input->get();
                if($query=$this->akun_model->get($data)){
                    if($query->num_rows()){
                        $response=$query->result_array();
                        for ($i=0; $i < sizeof($response); $i++) {
                            unset($response[$i]['password']);
                        }
                        $response=array($response);
                    }
                    else{
                        $response=array(array());
                    }
                    echo $this->api_model->response($response);
                }
                else{
                    echo $this->api_model->response(array(array('Internal Server Error'),500));
                }
                break;
            case 'post':
                $data=$this->input->post();
                if(!empty($this->user_session)){
                    switch($this->user_session['role']){
                        case 'admin':
                            $data['id_admin']=$this->user_session['id_user'];
                            break;
                        default:
                            echo $this->api_model->response(array(array('Error Auth'),401));
                            exit;
                            break;
                    }
                }
                if($this->akun_model->tambah($data)==1){
                    echo $this->api_model->response(array($data));
                }
                else{
                    echo $this->api_model->response(array(array('error'),200));
                }
                break;
            default:
                http_response_code(405);
        }
    }

    public function edit(){
        switch($this->input->method()){
            case 'post':
                if(!$this->input->post('id')) http_response_code(404);
                $data=array(
                    'param'=>$this->input->get(),
                    'data'=>$this->input->post()
                );

                if(!empty($this->user_session)){
                    switch($this->user_session['role']){
                        case 'user':
                            if($data['param']['id']!=$this->user_session['id_user']){
                                echo $this->api_model->response(array(array('Error Auth'),401));
                                exit;
                            }
                            break;
                        case 'admin':
                            break;
                        default:
                            echo $this->api_model->response(array(array('Error Auth'),401));
                            exit;
                            break;
                    }
                }
                if($this->akun_model->edit($data)){
                    echo $this->api_model->response(array($data));
                }
                else{
                    echo $this->api_model->response(array($data));
                }
                break;
            default:
                http_response_code(405);
        }
    }

    public function delete(){
        switch($this->input->method()){
            case 'post':
                if(!$this->input->post('id')) http_response_code(404);
                if(!empty($this->user_session)){
                    switch($this->user_session['role']){
                        case 'user':
                            if($this->input->post('id')!=$this->user_session['id_user']){
                                echo $this->api_model->response(array(array('Error Auth'),401));
                                exit;
                            }
                            break;
                        case 'admin':
                            break;
                        default:
                            echo $this->api_model->response(array(array('Error Auth'),401));
                            exit;
                            break;
                    }
                }
                else $id=$this->input->post('id');
        
                if($this->akun_model->delete($id)){
                    echo $this->api_model->response(array(array('Berhasil'),200));
                }
                else{
                    echo $this->api_model->response(array(array('error'),200));
                }
                break;
            default:
                http_response_code(405);
        }
    }

    public function login(){
        switch($this->input->method()){
            case 'post':
                $data=$this->input->post();
                if($this->akun_model->login($data)==1){
                    echo $this->api_model->response(array($data));
                }
                else{
                    echo $this->api_model->response(array(array('error'),200));
                }
                break;
            default:
                http_response_code(405);
        }
    }
}