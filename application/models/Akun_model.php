<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class Akun_model extends CI_Model{
        var $CI = NULL;
        var $table='user';
        var $valid_session=array(1,'HOUR');

        var $rules=array(
            array(
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ),
            array(
                'field' => 'nomor_hp',
                'label' => 'Nomor HP',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ),
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ),
            array(
                'field' => 'lokasi',
                'label' => 'Lokasi',
                'rules' => 'required'
            ),
            array(
                'field' => 'foto_profil',
                'label' => 'Foto Profil',
                'rules' => 'required'
            ),
            array(
                'field' => 'role',
                'label' => 'Role',
                'rules' => 'required'
            ),
        );
        public function __construct() {
            $this->CI =& get_instance();

            // $query=array(
            //     'expired'=>1
            // );
            // $this->destroySession($query);
        }

        function session_validation(&$data=NULL){
            if(!isset($data['token'])){
                http_response_code(404);
            }
            else{
                $this->db->where('token',$this->db->escape_str($data['token']));
                $this->db->limit(1);
                if($query=$this->db->get('token_auth')){
                    if($query->num_rows()){
                        $data=array_merge(
                            $query->result_array()[0],
                            array('message_api'=>'valid')
                        );
                        return true;
                    }
                    else{
                        return false;
                    }
                }
                else{
                    return false;
                }
            }
        }

        function cek_login($role=NULL){
            if($login=$this->session->userdata('login')){
                if(isset($role)){
                    if($role!=$login['role']){
                        if($this->session_validation($login['token'])==1){
                            return 1;
                        }
                        else{
                            return 0;
                        }
                    }
                    else{
                        return 0;
                    }
                }
                else{
                    return 1;
                }
            }
            else{
                return 0;
            }
        }

        function login(&$data=''){
            $rules=array(
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'required'
                )
            );
            $this->form_validation->set_data($data);
            $this->form_validation->set_rules($rules);
            
            if($this->form_validation->run()==true){
                $this->db->trans_begin();
                
                for ($i=0; $i < sizeof($rules); $i++) {
                    $allowed[]=$rules[$i]['field'];
                }
                
                $data['password']=md5($data['password']);
                
                foreach(array_keys($data) as $key){
                    switch($key){
                        case 'password':
                            break;
                        default:
                            if(in_array($key,$allowed)){
                                $data[$key]=$this->db->escape_str($data[$key]);
                            }
                            else{
                                return 0;
                            }
                    }
                }

                $this->db->limit(1);
                if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
                    $data['username']=$data['email'];
                    unset($data['email']);
                    $rules[0]['field']='username';
                    $rules[0]['label']='Username';
                }
                if($login=$this->db->get_where($this->table,$data)){
                    if($login->num_rows()>0){
                        $login=$login->result_array()[0];

                        // pembuatan session
                        $data=array(
                            'id_user'=>$login['id'],
                            'ip_address'=>$_SERVER['REMOTE_ADDR'],
                            'role'=>$login['role'],
                            'timestamp_created'=>date("Y-m-d H:i:s")
                        );
                        $data=array_merge(array('token'=>md5($_SERVER['REMOTE_ADDR'].'_'.$login['id'].'_'.$login['role'].'_'.$data['timestamp_created'])),$data);
                        if($this->db->insert('token_auth',$data)){
                            if($this->db->trans_status()===true){
                                $this->db->where('id',$data['id_user']);
                                $this->db->select('email');
                                $this->db->limit(1);
                                $user_detail=$this->db->get($this->table);
                                if($user_detail->num_rows()){
                                    $data=array_merge(
                                        $data,
                                        $user_detail->result_array()[0]
                                    );
                                }
                                else{
                                    $this->db->trans_rollback();
                                    return 0;
                                }
                                $this->session->set_userdata('login',$data);
                                $data['message']='success';
                                $this->db->trans_commit();
                                return 1;
                            }
                            else{
                                $this->db->trans_rollback();
                                return 0;
                            }
                        }
                    }
                    else{
                        return 0;
                    }
                }
                else{
                    return 0;
                }
            }
            else{
                return 0;
            }
        }

        function logout($token=NULL){
            if(!isset($token)){
                $token=$this->session->userdata('login')['token'];
            }

            session_destroy();

            $query=array(
                'param'=>array(
                    'token'=>$token
                )
            );
            if($this->destroySession($query)==1){
                return 1;
            }
            else{
                return 0;
            }
        }

        function getRole($data=NULL){
            if(isset($data)){
                if(isset($data['param'])){
                    foreach(array_keys($data['param']) as $key){
                        $this->db->where($key, $this->db->escape_str($data['param'][$key]));
                    }
                }
    
                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }
            }

            $query=$this->db->get('user_role');

            return $query;
        }

        function getSession($data=NULL){
            if(isset($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('id','nama');
                        foreach($allowed as $param){
                            if(isset($data['param'][$param])){
                                $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                            }
                        }
                    }
                }

                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }

                if(isset($data['expired'])){
                    switch($data['expired']){
                        case 0:
                            $this->db->where('DATE_ADD(timestamp_created, INTERVAL '.$this->valid_session[0].' '.$this->valid_session[1].')<CURENT_TIMESTAMP');
                            break;
                        case 1:
                            $this->db->where('DATE_ADD(timestamp_created, INTERVAL '.$this->valid_session[0].' '.$this->valid_session[1].')>=CURENT_TIMESTAMP');
                            break;
                        default:
                            return 0;
                    }
                }
            }

            $query=$this->db->get('login_session');
        }

        function destroySession($data=NULL){

            $this->db->trans_begin();

            if(isset($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('id','nama');
                        foreach($allowed as $param){
                            if(isset($data['param'][$param])){
                                $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                            }
                        }
                    }
                }

                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }

                if(isset($data['expired'])){
                    switch($data['expired']){
                        case 0:
                            $this->db->where("DATE_ADD(timestamp_created, INTERVAL ".$this->valid_session[0]." ".$this->valid_session[1].")>="."'".date('Y-m-d H:i:s')."'");
                            break;
                        case 1:
                            $this->db->where("DATE_ADD(timestamp_created, INTERVAL ".$this->valid_session[0]." ".$this->valid_session[1].")<"."'".date('Y-m-d H:i:s')."'");
                            break;
                    }
                }
            }
            
            if($this->db->delete('login_session')){
                if($this->db->trans_status()===true){
                    $this->db->trans_commit();
                    return 1;
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
        }

        function get($data=NULL){
            if(isset($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('id','nama');
                        foreach($allowed as $param){
                            if(isset($data['param'][$param])){
                                $this->db->where($param,$this->db->escape_str($data['param'][$param]));
                            }
                        }
                    }
                }

                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                    unset($data['limit']);
                }

                $allowed=array('id','nama','email','nomor_hp','role');
                foreach($allowed as $param){
                    if(isset($data[$param])){
                        $this->db->where($param,$this->db->escape_str($data[$param]));
                    }
                }
            }

            $query=$this->db->get($this->table);

            return $query;
        }

        function tambah(&$data){
            $rules=array_merge($this->rules);
            $this->form_validation->set_data($data);
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run()==true){

                foreach($rules as $rule){
                    if($rule['field']!='role'){
                        $allowed[]=$rule['field'];
                    }
                }
                $data['password']=md5($data['password']);
                $data['role']=strtolower($data['role']);

                foreach(array_keys($data) as $key){
                    if(in_array($key,$allowed)){
                        if(isset($data['param'][$key])){
                            $data[$key]=$this->db->escape_str(htmlspecialchars($data[$key]));
                        }
                    }
                    else{
                        unset($data[$key]);
                    }
                }

                $this->db->trans_begin();

                if($this->db->insert($this->table,$data)){
                    if($this->db->trans_status()===true){
                        $data=array_merge(array('id'=>$this->db->insert_id()),$data);
                        $this->db->trans_commit();
                        return 1;
                    }
                    else{
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                for ($i=0; $i < sizeof($this->rules); $i++) {
                    echo form_error($this->rules[$i]['field']);
                }
                return 0;
            }
        }

        function edit(&$data){
            if(isset($data['data']) && isset($data['param'])){
                $data['message_api']='';
                unset($this->rules[1]);
                $rules=array_merge($this->rules);
                
                $allowed=array('id','email','nomor_hp','username');
                foreach(array_keys($data['param']) as $key){
                    if(in_array($key,$allowed)){
                        $data['param'][$key]=$this->db->escape_str($data['param'][$key]);
                    }
                    else{
                        unset($data['param'][$key]);
                    }
                }

                $allowed=array('nama','nomor_hp','email','username','password','lokasi','foto_profil','role');
                foreach(array_keys($data['data']) as $key){
                    if(in_array($key,$allowed)){
                        if($key=='role'){
                            $data['data']['role']=strtolower($data['data']['role']);
                        }
                        $data['data'][$key]=$this->db->escape_str($data['data'][$key]);
                    }
                    else{
                        unset($data['data'][$key]);
                    }
                }

                $this->db->trans_begin();

                foreach(array_keys($data['param']) as $key){
                    $this->db->where($key,$data['param'][$key]);
                }
                if($this->db->update($this->table,$data['data'])){
                    if($this->db->trans_status()==true){
                        if($this->db->affected_rows()>0){
                            $data['message_api']='Success';
                            $this->db->trans_commit();
                            return 1;
                        }
                        else{
                            $this->db->trans_rollback();
                            $data['message_api']='Transaksi gagal';
                            return 0;
                        }
                    }
                    else{
                        $this->db->trans_rollback();
                        $data['message_api']='Transaksi gagal';
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    $data['message_api']='Internal Server Error';
                    return 0;
                }
            }
            else{
                return 0;
            }
        }

        function delete(&$id){
            if(isset($id)){
                $this->db->trans_begin();
                $this->db->where('id',$this->db->escape_str($id));
                if($this->db->delete($this->table)){
                    if($this->db->affected_rows()>0){
                        if($this->db->trans_status()===true){
                            $this->db->trans_commit();
                            return 1;
                        }
                        else{
                            $this->db->trans_rollback();
                            return 0;
                        }
                    }
                    else{
                        $this->db->trans_rollback();
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                return 0;
            }
        }
    }
?>