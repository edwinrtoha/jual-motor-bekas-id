<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class Iklan_model extends CI_Model{
        var $CI = NULL;
        var $table='iklan';

        var $rules=array(
            array(
                'field' => 'judul',
                'label' => 'Judul',
                'rules' => 'required'
            ),
            array(
                'field' => 'deskripsi',
                'label' => 'Deskripsi',
                'rules' => 'required'
            ),
            array(
                'field' => 'jenis_iklan',
                'label' => 'Jenis Iklan',
                'rules' => 'required'
            ),
            array(
                'field' => 'harga',
                'label' => 'Harga',
                'rules' => 'required'
            ),
            array(
                'field' => 'kondisi',
                'label' => 'Kondisi',
                'rules' => 'required|in_list[baru,bekas]'
            ),
            array(
                'field' => 'merek',
                'label' => 'Merek',
                'rules' => 'required'
            ),
            array(
                'field' => 'tahun',
                'label' => 'Tahun',
                'rules' => 'required|numeric'
            )
        );
        public function __construct() {
            $this->CI =& get_instance();
        }

        function get($data=''){
            if(!empty($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('judul','deskripsi','jenis_iklan','harga','kondisi','merek','tahun');
                        foreach($allowed as $key){
                            if(in_array($key,$allowed)){
                                if(isset($data['param'][$key])){
                                    $this->db->where($key,$this->db->escape_str($data['param'][$key]));
                                }
                            }
                            else{
                                unset($data['param'][$key]);
                            }
                        }
                    }
                }
    
                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }
            }

            $query=$this->db->get($this->table);

            return $query;
        }

        function tambah(&$data){
            $this->form_validation->set_data($data);
            $this->form_validation->set_rules($this->rules);
            
            if($this->form_validation->run()==true){
                $allowed=array('judul','deskripsi','jenis_iklan','harga','kondisi','merek','tahun');
                foreach(array_keys($data) as $key){
                    if(in_array($key,$allowed)){
                        if(isset($data['param'][$key])){
                            $data[$key]=$this->db->escape_str(htmlspecialchars($data[$key]));
                        }
                    }
                    else{
                        unset($data[$key]);
                    }
                }
                $this->db->trans_begin();

                if($this->db->insert($this->table,$data)){
                    if($this->db->trans_status()===true){
                        $data=array_merge(array('id'=>$this->db->insert_id()),$data);
                        $this->db->trans_commit();
                        return 1;
                    }
                    else{
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                return 0;
            }
        }

        function edit(&$data){
            if(isset($data['data']) && isset($data['param'])){
                $data['message_api']='';
                unset($this->rules[1]);
                $rules=array_merge($this->rules);
                
                $allowed=array('id','jenis_iklan');
                foreach(array_keys($data['param']) as $key){
                    if(in_array($key,$allowed)){
                        $data['param'][$key]=$this->db->escape_str($data['param'][$key]);
                    }
                    else{
                        unset($data['param'][$key]);
                    }
                }

                $allowed=array('judul','deskripsi','jenis_iklan','harga','kondisi','merek','tahun');
                foreach(array_keys($data['data']) as $key){
                    if(in_array($key,$allowed)){
                        if($key=='role'){
                            $data['data']['role']=strtolower($data['data']['role']);
                        }
                        $data['data'][$key]=$this->db->escape_str($data['data'][$key]);
                    }
                    else{
                        unset($data['data'][$key]);
                    }
                }

                $this->db->trans_begin();

                foreach(array_keys($data['param']) as $key){
                    $this->db->where($key,$data['param'][$key]);
                }
                if($this->db->update($this->table,$data['data'])){
                    if($this->db->trans_status()==true){
                        if($this->db->affected_rows()>0){
                            $data['message_api']='Success';
                            $this->db->trans_commit();
                            return 1;
                        }
                        else{
                            $this->db->trans_rollback();
                            $data['message_api']='Transaksi gagal';
                            return 0;
                        }
                    }
                    else{
                        $this->db->trans_rollback();
                        $data['message_api']='Transaksi gagal';
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    $data['message_api']='Internal Server Error';
                    return 0;
                }
            }
            else{
                return 0;
            }
        }
        
        function delete($id){
            switch($this->input->method()){
                case 'post':
                    if(isset($id)){
                        $this->db->trans_begin();
                        $this->db->where('id',$this->db->escape_str($id));
                        if($this->db->delete($this->table)){
                            if($this->db->affected_rows()>0){
                                if($this->db->trans_status()===true){
                                    $this->db->trans_commit();
                                    return 1;
                                }
                                else{
                                    $this->db->trans_rollback();
                                    return 0;
                                }
                            }
                            else{
                                $this->db->trans_rollback();
                                return 0;
                            }
                        }
                        else{
                            $this->db->trans_rollback();
                            return 0;
                        }
                    }
                    else{
                        return 0;
                    }
                    break;
                default:
                    http_response_code(405);
            }
        }
    }
?>