<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class WHistlist_model extends CI_Model{
        var $CI = NULL;
        var $table='whistlist';

        var $rules=array(
            array(
                'field' => 'id_iklan',
                'label' => 'ID Iklan',
                'rules' => 'required'
            ),
            array(
                'field' => 'id_user',
                'label' => 'ID User',
                'rules' => 'required'
            )
        );
        public function __construct() {
            $this->CI =& get_instance();
        }

        function get($data=''){
            if(!empty($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('id_iklan','id_user');
                        foreach($allowed as $key){
                            if(in_array($key,$allowed)){
                                if(isset($data['param'][$key])){
                                    $this->db->where($key,$this->db->escape_str($data['param'][$key]));
                                }
                            }
                            else{
                                unset($data['param'][$key]);
                            }
                        }
                    }
                }
    
                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }
            }

            $query=$this->db->get($this->table);

            return $query;
        }

        function tambah(&$data){
            $this->form_validation->set_data($data);
            $this->form_validation->set_rules($this->rules);
            
            if($this->form_validation->run()==true){
                $allowed=array('id_iklan','id_user');
                foreach(array_keys($data) as $key){
                    if(in_array($key,$allowed)){
                        if(isset($data['param'][$key])){
                            $data[$key]=$this->db->escape_str(htmlspecialchars($data[$key]));
                        }
                    }
                    else{
                        unset($data[$key]);
                    }
                }
                $this->db->trans_begin();

                if($this->db->insert($this->table,$data)){
                    if($this->db->trans_status()===true){
                        $this->db->trans_commit();
                        return 1;
                    }
                    else{
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                return 0;
            }
        }
        
        function delete(&$data){
            $this->form_validation->set_data($data);
            $this->form_validation->set_rules($this->rules);
            if($this->form_validation->run()==true){
                $this->db->trans_begin();
                $this->db->where('id_iklan',$this->db->escape_str($data['id_iklan']));
                $this->db->where('id_user',$this->db->escape_str($data['id_user']));
                if($this->db->delete($this->table)){
                    if($this->db->affected_rows()>0){
                        if($this->db->trans_status()===true){
                            $this->db->trans_commit();
                            return 1;
                        }
                        else{
                            $this->db->trans_rollback();
                            return 0;
                        }
                    }
                    else{
                        $this->db->trans_rollback();
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                return 0;
            }
            if(isset($id)){
                $this->db->trans_begin();
                $this->db->where('id',$this->db->escape_str($id));
                if($this->db->delete($this->table)){
                    if($this->db->affected_rows()>0){
                        if($this->db->trans_status()===true){
                            $this->db->trans_commit();
                            return 1;
                        }
                        else{
                            $this->db->trans_rollback();
                            return 0;
                        }
                    }
                    else{
                        $this->db->trans_rollback();
                        return 0;
                    }
                }
                else{
                    $this->db->trans_rollback();
                    return 0;
                }
            }
            else{
                return 0;
            }
        }
    }
?>