<?php
    if ( ! defined("BASEPATH")) exit("No direct script access allowed");
    class Api_model extends CI_Model{
        var $CI = NULL;
        public function __construct() {
            $this->CI =& get_instance();
        }

        function get_allow_origin(){
            header('Access-Control-Allow-Origin: *');
        }

        function response($response){
            if(is_array($response)){
                if(sizeof($response)==2){
                    $respon=json_encode($response[0],JSON_PRETTY_PRINT);
                    http_response_code($response[1]);
                }
                else{
                    $respon=json_encode($response[0],JSON_PRETTY_PRINT);
                    http_response_code(200);
                }
            }
            else{
                $respon=json_encode(array('error'));
                http_response_code(204);
            }
            return $respon;
        }

        function getAuthorizationHeader(){
            $headers = null;
            if (isset($_SERVER['Authorization'])) {
                $headers = trim($_SERVER["Authorization"]);
            }
            else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
                $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
            } elseif (function_exists('apache_request_headers')) {
                $requestHeaders = apache_request_headers();
                // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
                $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
                //print_r($requestHeaders);
                if (isset($requestHeaders['Authorization'])) {
                    $headers = trim($requestHeaders['Authorization']);
                }
            }
            return $headers;
        }

        function getToken() {
            $headers = $this->getAuthorizationHeader();
            // HEADER: Get the access token from the header
            if (!empty($headers)) {
                if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                    return $matches[1];
                }
            }
            return null;
        }

        function getTokenDetail(&$data=''){
            if(!empty($data)){
                if(isset($data['param'])){
                    if(is_array($data['param'])){
                        $allowed=array('token','id_user');
                        foreach($allowed as $key){
                            if(in_array($key,$allowed)){
                                if(isset($data['param'][$key])){
                                    $this->db->where($key,$this->db->escape_str($data['param'][$key]));
                                }
                            }
                            else{
                                unset($data['param'][$key]);
                            }
                        }
                    }
                }
    
                if(isset($data['limit'])){
                    if(is_array($data['limit'])){
                        if(sizeof($data['limit'])==2){
                            $this->db->limit($data['limit'][0]>0 ? $data['limit'][0] : 1, $data['limit'][1]>0 ? $data['limit'][1] : 1);
                        }
                        else{
                            $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                        }
                    }
                    else{
                        $this->db->limit($data['limit']>0 ? $data['limit'] : 1);
                    }
                }
            }

            if($query=$this->db->get('token_auth')){
                if($query->num_rows()>0){
                    $data=$query->result_array();
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
    }
?>